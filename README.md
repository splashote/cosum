# Cosum - a Laravel based lending platform

Cosum is a lending platform. Use it to manage your items and lend them.

This application is still in the beta stage and was initially meant to be a demo and was produced by one man alone.

That is, loss of data might occur due to changes of the data structures beneath.

The software is GPL licensed. Therefore can be downloaded, run, changed and redistributed as you wish.

Make changes and let us know what you did. We will be happy about merge requests.


The application is based on laravel framework.
Special thanks to the framework contributors!

## Version v0.8.6hf1

## Getting started
`composer install`

In case of Class related issues
`composer dump-autoload` is of help.

Then make a copy of the env sample file.
However, you should never save local credentials/secrets of that file on the repository once they are entered.

`cp .env.example .env`

Edit the .env as you see fit.

## Database
The database connection is also in that .env file.

In case you haven't already, don't forget to create a new db, with a user who has access and is granted the needed privileges.

That user will do transactions on your behalf.

When the databases is set up and running you need to run the migrations.

`php artisan migrate`

and

`php artisan db:seed`

If something unexpected happens run  
`php artisan migrate:status` and check which of the migrations where cancelled.

## Folder permissions
Make sure the temporary folders are writeable such as storage/bootstrap or storage/cache

## Webserver
install a webserver of your liking and have it point to the public folder of this app.

For local testing you may also run:  php artisan serve

## The first User
The very first user needs to be created manually.

Run `php artisan tinker` and do something like `$u1=App\User::create(<params>);`
Also do  `$u1->operator = now();`
An operator is a user who is has access to the blueboard, which provides overview over the platform.
There he may create registration keys, which are necessary to have people register.
Check if he really is: `$u1->isOperator();`
Save this assigment: `$su1->save();`

Lookup Laravel for help on the tinker shell.


## Have fun!
Help us create a sustainable world!
