@extends('layouts.app')

@section('content')
<div class="container" id="registration_container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading">Willkommen bei Cosum!</div>
                <div class="panel-body reddish_text">
                <p>
<strong>Neu hier?</strong> Dann schick uns eine Mail an <a href="mailto:gerard@cosum.de?Subject=Schlüssel" target="_top">gerard(at)cosum.de</a> oder <a href="mailto:nikolai@cosum.de?Subject=Schlüssel" target="_top">nikolai(at)cosum.de</a>
mit kurzer Beschreibung deiner Gemeinschaft. Du bekommst von uns dann einen <strong>Schlüssel</strong>, den du für die Registrierung brauchst. Wir freuen uns auf dich!
                </p>
                </div>
            </div>
        <h2>Registrierung</h2>
        </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading">Gemeinschaft</div>
                <div class="panel-body">
                        <div class="form-group{{ $errors->has('keyname') ? ' has-error' : '' }}">
                            <label for="keyname" class="col-md-4 control-label">Schlüsselname</label>
                            <div class="col-md-6">
                                <input id="keyname" type="text" class="form-control" name="keyname" value="{{ old('keyname') }}" required>

                                @if ($errors->has('keyname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keyname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('keycode') ? ' has-error' : '' }}">
                            <label for="keycode" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="keycode" type="text" class="form-control" name="keycode" value="{{ old('keycode') }}" required>

                                @if ($errors->has('keycode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('keycode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="auto_connect" class="col-md-4 control-label">Auto-Verbindung</label>
                            <div class="col-md-6">
                                {{Form::hidden('auto_connect',0)}}
                                {{Form::checkbox('auto_connect')}}
                            <p>Ich möchte mit Gemeinschaftsmitgliedern automatisch verbunden werden. <br/> Der unten stehende Name wird bei einer Verbindung einmalig bekannt gegeben.</p>
                            </div>
                        </div>

                </div>
                </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default panel-warning">
                <div class="panel-heading">Persönliche Angaben</div>
                <div class="panel-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail-Adresse</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <password-component ref="pw_comp" be_error="{{ $errors->first('password') }}" ></password-component> 
                         </div>
            </div>
        </div>

            <div class="col-md-6 col-md-offset-2">
                <button type="submit" class="btn btn-success front-btn">
                  abschicken
                </button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@section ('page_script_code')

@endsection

