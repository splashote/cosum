@extends('layouts.app')

@section('content')
    <div class="row-fluid">
        <div class="col-md-12">
            <div class="jumbotron comic_item_back text-center">
                <p>
                <span ><img style="width:100%" src="/img/cosum_logo_and_claim_desat.png" /> </span>
                </p>
                <p style="color:white;font-weight:bold;">Die Plattform zum nachhaltigen Leihen und Schenken</p>
                @if (Auth::guest())
                <p>
                    <a class="btn btn-primary front-btn" href="/gegenstaende" role="button" style="">zu den Gegenständen</a>
                </p>
                <p>
                    <a class="btn btn-success front-btn" href="/login" role="button">anmelden</a>
                    <a class="btn btn-success front-btn" href="/register" role="button">registrieren</a>
                </p>
                @endif
            </div>

	    <div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		  </ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">

		    <div class="jumbotron text-center mini_story smart_woman item active">
			<p>
			<span > Ist er wirklich wieder an meinem Fön interessiert? </span>
			</p>
		    </div>
		    <div class="jumbotron text-center mini_story backpacker item ">
			<p>
			<span > Der Rucksack hat mich weit gebracht. Danke COSUM. </span>
			</p>
		    </div>
		    <div class="jumbotron text-center mini_story laptop_woman item">
			<p>
			<span > Ersatz für meinen kaputten Laptop und meine Doktorarbeit ist gerettet. Danke COSUM. </span>
			</p>
		    </div>
		</div>

	      <!-- Left and right controls -->
	      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
		<span class="sr-only">Previous</span>
	      </a>
	      <a class="right carousel-control" href="#myCarousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
		<span class="sr-only">Next</span>
	      </a>
	   </div>

        </div>
        <div class="col-md-12">
            <p class="text-center lending_concept separator"><img class="" height="50px;" src="/img/vin_tennisschlaeger.png" alt="" /> </p>
        </div>

        <div class="col-md-12">
            <h1>Warum Teilen so wichtig ist:</h1>
        </div>
        <div class="col-md-4">
            <div class="jumbotron text-center item_example">
            <a href="#text_verbindet" data-toggle="collapse" >
                <p class="header_link">
                <span class="glyphicon glyphicon-link"></span><br />
                Teilen verbindet.
                </p>
            </a>
            <p class="collapse" id="text_verbindet">
                Als COSUMER teilst du Gegenstände mit den Menschen, die du auch wirklich treffen willst!
            </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="jumbotron text-center item_example" >
            <a href="#text_umweltschonung" data-toggle="collapse" >
                <p class="header_link">
                <span class="glyphicon glyphicon-globe"></span><br />
                Teilen schont die Umwelt.
                </p>
            </a>
            <p class="collapse" id="text_umweltschonung">
                Wegwerfen war gestern!<br />
                Leihen verringert den Resourcen-Verbrauch.<br />
                Denn was schon da ist, muss nicht mehr produziert werden!
            </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="jumbotron text-center item_example">
            <a href="#text_spart" data-toggle="collapse" >
                <p class="header_link">
                <span class="glyphicon glyphicon-euro"></span><br />
                Teilen spart Geld.
                </p>
            </a>
            <p class="collapse" id="text_spart">
                Warum kaufen, was du schon zur Verfügung hast? <br />
                Tu' deinem Geldbeutel was gutes!
            </p>
            </div>
        </div>

        <div class="col-md-12">
            <p class="text-center lending_concept separator"><img class="" height="50px;" src="/img/vin_buecher.png" alt="" /> </p>
        </div>
        <div class="col-md-12">
            <h1>Ja, wo leihen sie denn?</h1>
        </div>
        <div class="col-md-6">
            <div class="jumbotron text-center lending_concept">
            <h3>Leihen von Mensch zu Mensch</h3>
            <p>
Cosum hilft dabei, das Verleihen zwischen Menschen, die in Kontakt zueinander stehen, zu organisieren. Über Gruppierungen lassen sich auch Gemeinschaften abbilden.
            </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="jumbotron text-center lending_concept">
            <h3>Leihen im Leihladen</h3>
            <p>
Ein Leihladen ist ein Ort, in dem Gegenstände wie in einer Bibliothek aufbewahrt und verliehen werden.
            <br />
            </p>
            </div>
        </div>

        <div class="col-md-12">
            <p class="text-center lending_concept separator" ><img class="" height="50px;" src="/img/vin_bohrmachine.png" alt="" /> </p>
        </div>
        <div class="col-md-12">
            <h2>COSUM braucht deinen Beitrag!</h2>
        </div>
        <div class="col-md-6">
            <div class="jumbotron text-center item_example">
            <p>Damit wir die Plattform weiterentwickeln können und auch lokale Leihprojekte mit Kursen empowern können, benötigen wir deine finanzielle Unterstützung. Jeder Euro hilft uns.</p>
            <p>
                Spenden auf das GeLa Konto <br />
                (Die Gemeinnützigkeit wurde beantragt)<br />
                IBAN: DE66 4306 0967 1119 8462 00<br />
                BIC: GENODEM1GLS<br />
            </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="jumbotron text-center item_example">
            <p>Entwickler gern gesehen! <br />
                Über die Gitlab Platform ist der Quelltext vergangener Versionen öffentlich verfügbar.
                Wir freuen uns über jeden Pull-Request. <br />
                <a class="btn btn-lg btn-info front-btn top-buffer" href="https://gitlab.com/dialogium/cosum" target="_top">OpenSource!</a>
            </p>

            </div>
        </div>

    </div>
@endsection
