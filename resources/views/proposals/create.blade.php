@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <a href="/proposals" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>Vereinbarungstext hinzufügen</h1>
    {!! Form::open(['action' => 'ProposalsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Titel')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Text')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>
        {{Form::submit('Speichern', ['class'=>'btn btn-sm btn-info front-btn'])}}
    {!! Form::close() !!}
@endsection
