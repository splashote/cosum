@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-12">
                    <div class="form-group">
                    @component('search.comp',['search' => $search])
                    @endcomponent
                    </div>
            </div>
        </div>
        <h3>Ergebnisse:</h3>
                @if (isset($gegenstaende) && count($gegenstaende) > 0)
                        @foreach ($gegenstaende as $gegenstand)
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                            @if ($gegenstand->gift)
                                                <span class="giftbanner btn-sm btn-default btn-warning active input-group-addon" >Zu verschenken</span>
                                            @endif
                                        <img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
                                           @if ($gegenstand->category)
                                            <span class="category_lbl btn-sm btn-default btn-info">
                                                    <b><label>{{$gegenstand->category}}</label></b>
                                            </span>
                                            @endif
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <h3><a href="/gegenstaende/{{$gegenstand->id}}">{{$gegenstand->name}}</a></h3>
                                        <strong><a href="/orte/{{$gegenstand->ort_id}}">{{$gegenstand->ort_name}}</a></strong><br/>
                                        <small>Ersteintragung am {{$gegenstand->created_at}}</small>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        @if ($gegenstand->lent)
                                            <span class="verleih_status btn-sm btn-default btn-danger active">Verliehen</span>
                                            <div class='input-group date top-buffer' >
                                                <span class="input-group-addon">
                                                    <label for="lent_to_date">Bis:</label>
                                                </span>
                                                <span class="input-group-addon">
                                                    <label for="lent_to_date">{{$gegenstand->lent_to_date}}</label>
                                                </span>
                                            </div>
                                        @else
                                            <span class="verleih_status btn-sm btn-default btn-success active">Verfügbar</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{$gegenstaende->links()}}
             @elseif ($search)
                    <p> Leider nichts gefunden. </p>
             @else
                    <p></p>
             @endif
</div>
@endsection
