            {!! Form::open(['url' => '/search', 'id' => 'searchform', 'class'=>'form searchform', 'style' => 'margin-bottom:15px;margin-top:-23px;']) !!}
                <span class="input-group-addon" onclick='var sf=document.getElementById("searchform");console.log("sf:"+sf); sf.submit();' style="cursor:pointer;" >
                    <span  class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </span>
                @php
                    $old=(isset($search))?$search:"";
                @endphp
                {!! Form::text('search', $old , ['required', 'class'=>'form-control input-group-text searchinput', 'placeholder'=>'Suchbegriff']) !!}
            {!! Form::close() !!}
