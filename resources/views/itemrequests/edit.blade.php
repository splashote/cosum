@extends ('layouts.app')

@section ('content')
    <a href="/gesuche" class="btn btn-primary">zurück</a>
    <h1>Gesuch bearbeiten</h1>
    {!! Form::open(['action' => ['ItemRequestsController@update', $itemrequest->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Titel')}}
            {{Form::text('title', $itemrequest->title, ['class' => 'form-control', 'placeholder' => 'Titel'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Text')}}
            {{Form::textarea('body', $itemrequest->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
