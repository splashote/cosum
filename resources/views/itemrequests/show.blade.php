@extends ('layouts.app')

@section ('content')
    <a href="/gesuche" class="btn btn-primary">zurück</a>
    <h1>{{$itemrequest->title}}</h1>
    <br><br>
    <div class="well gesuchotron">
        {!!$itemrequest->body!!}
    </div>
    <hr>

    @if (Auth::user()->id == $itemrequest->user_id)
    <a href="/gesuche/{{$itemrequest->id}}/edit" class="btn btn-primary">bearbeiten</a>
    @endif
@endsection
