@extends('layouts.app')

@section('content')
    <h1>Kontaktkreis anlegen</h1>
    {!! Form::open(['action' => 'ContactCirclesController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        {{Form::submit('anlegen', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
