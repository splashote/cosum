@extends('layouts.app')
@section('content')
<h1>"{{$gegenstand->name}}" offenlegen</h1>
	 <div class="row">
	    <div class="col-lg-3 col-md-3 col-sm-3">
		<img style="width:100%" src="/storage/cover_images/{{$gegenstand->cover_image}}">
            </div>
        </div>
<p class="alert alert-info">Wem soll der Gegenstand offengelegt werden?</p>
<h2>Kontaktkreise</h2>
@if (count($contactcircles) > 0)
    @foreach ($contactcircles as $contactcircle)
        <a class="tag label label-primary" href="/offenlegungen/kreis/{{$contactcircle->id}}/?obj_id={{$gegenstand->id}}">{{$contactcircle->name}}</a>
    @endforeach
@else
        <p>Keine</p>
@endif
<hr/>
<h2>Verbundene Kontakte</h2>
     @if (count($contacts) > 0)
        @foreach ($contacts as $contact)
                <a class="tag label label-success" href="/offenlegungen/create?obj_id={{$gegenstand->id}}&rcpt_id={{$contact->id}}">{{$contact->called}}</a>
        @endforeach
        {!! Form::open(['id' => 'selectionForm', 'url' => $returnAddress]) !!}
            {{Form::hidden('new_contact_id', '', ['hidden' => 'hidden','class' => 'form-control', 'placeholder' => 'Kontakt id'])}}
        {!! Form::close() !!}
    @else
        <p>Keine Verbindungen</p>
    @endif
@endsection
