@extends('layouts.app')

@section('content')
        <h2>Profil</h2>
        <div class="row">
            <div class="col-sm-6">
            Benutzername:
                <strong>
                    {{ auth()->user()->name }}
                </strong>
            <br />
            Registrierte Email-Adresse:
                <strong>
                    {{ auth()->user()->email }}
                </strong>
            <br />
            <strong>Beschreibungstext:</strong><br />
            <div class="well">
            {!! auth()->user()->profile->description !!}
            </div>
                <a class="btn btn-warning" href="/profil/edit">
                    Bearbeiten
                </a>
            </div>
            <div class="col-sm-12">
            <hr />
                <p>Vielen Dank für das Verleihen von Gegenständen. <br />
Dadurch ergibt sich eine persönliche geschätzte Gesamt-Einsparung von <strong>{{ $total_savings }} Gramm CO2e</strong>.</p>
                </p>
            </div>
	</div>
@endsection
