@extends('layouts.app')

@section('content')
        <h2>Profil</h2>
        <div class="row">
            <div class="col-sm-6">
            Benutzername:
                <strong>
                    {{ auth()->user()->name }}
                </strong>
            <br />
            Registrierte Email-Adresse:
                <strong>
                    {{ auth()->user()->email }}
                </strong>
                {!! Form::open(['action' => ['ProfileController@email_set'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="">
                                <input id="old_email" type="hidden" class="form-control" name="old_email" value="{{ auth()->user()->email }}" hidden="hidden" required>
                                <label for="email" class=" control-label">Neue E-Mail-Adresse</label>
                                <input id="email" type="email" class="form-control" name="email" value="" required>
                                <label for="email_confirmation" class=" control-label">Bestätigen</label>
                                <input id="email_confirmation" type="email" class="form-control" name="email_confirmation" value="" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                <p>Durch den Klick auf "ändern" wird der <strong>Login blockiert </strong>und die neue Email-Adresse muss verifiziert werden. Die alte <strong>Email-Adresse wird gelöscht</strong>.</p>
                {{Form::submit('ändern', ['class'=>'btn btn-primary'])}}
            {!! Form::close() !!}
            </div>
            <div class="col-sm-6">
                {!! Form::open(['action' => ['ProfileController@update'],  'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <div class="form-group">
                    {{Form::label('Beschreibungstext', 'Wer bin ich?')}}
                    {{Form::textarea('Beschreibungstext', $profile->description, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Beschreibungstext'])}}
                </div>
                {{Form::submit('speichern', ['class'=>'btn btn-primary'])}}
                {!! Form::close() !!}
            </div>

	</div>
@endsection
