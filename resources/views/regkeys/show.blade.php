@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
    <div class="row">
    <a href="/regkeys" class="btn btn-sm btn-default btn-info front-btn">Zurück</a>
    <h1>{{$regkey->keyname}}</h1>
    <br><br>
    <div class="well">
        {!!$regkey->keycode!!}
    </div>
        <div class="col-xs-12">
            {{Form::label('description', 'Beschreibung:')}}
            {!!$regkey->description!!}
        </div>

    <hr>
    <small>Verfasst am {{$regkey->created_at}} von {{$regkey->creator->name}}</small>
    <hr>
    <a href="/regkeys/{{$regkey->id}}/edit" class="btn btn-sm btn-info front-btn">bearbeiten</a>
    </div>
@endsection
