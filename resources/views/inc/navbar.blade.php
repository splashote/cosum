<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="brandname navbar-brand" href="{{ url('/') }}">
                <img src="/img/cosum_logo_and_claim_desat.png" width="200px"/>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <ul class="nav navbar-nav">
              @if (Auth::guest())
                <li><a href="/gegenstaende">Gegenstände</a></li>
              @else
              <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                           Gegenstände<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                                <li><a href="/gegenstaende">öffentliche</a></li>
                                <li><a href="/kommunal_gegenstaende">kommunale</a></li>
                                <li><a href="/offenlegungen">offengelegte</a></li>
                        </ul>
              </li>
              <li><a href="/gesuche">Gesuche</a></li>
              <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                           Kontakt<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                          <li><a href="/kontakte">Liste</a></li>
                          <li><a href="/kontaktkreise">Kreise</a></li>
                        </ul>
            </li>

            @endif
            <li>
                <a class="" href="http://cosum-blog.de">Blog</a>
            </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right" style="font-size:20px;">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">anmelden</a></li>
                    <li><a href="{{ route('register') }}">registrieren</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span>
                            <strong>  {{ Auth::user()->name }} </strong>
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            {{-- <li><a href="/dashboard">Dashboard</a></li> --}}
                            <li><a href="/meine_gegenstaende">Meine Gegenstände</a></li>
                            <li><a href="/meine_offenlegungen">Offenlegungen</a></li>
	                    <li><a href="/orte">Orte</a></li>
	                    <li><a href="/profil" class="menu_sep" >Profil</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                    abmelden
                                   <span class="glyphicon glyphicon-off"></span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
