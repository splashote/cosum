<div class="pinned_note" style="padding-left:43px;">
        <a href="#portalinfo" class="version" data-toggle="collapse">
                <strong> {{ config('app.version', 'temp' ) }} </strong>
        </a>
	<p class="version collapse" id="portalinfo">
                Cosum:  <strong> {{ config('app.version', 'temp' ) }} </strong>
            <a href="/dokumente/CHANGELOG" class="glyphicon glyphicon-tag"></a>,
                Laravel:<strong> {{ App::VERSION() }} </strong>,
                PHP:    <strong>
                    @php
                        $php_version_complete=phpversion();
                        $phpversion=explode('-',$php_version_complete);
                        echo $phpversion[0];
                    @endphp
                    </strong>,
            <a href="/blueboard" class="glyphicon glyphicon-blackboard"></a>
	</p>
</div>
<div class="footer">
    <div class="row">
        <div class="col-md-3">

        <h1>Kontakt</h1>
        <p>
        <a class="btn btn-xs btn-info front-btn top-buffer" href="mailto:nikolai@cosum.de?Subject=[Cosum] Kontakt" target="_top">nikolai(at)cosum.de</a><br />
        <a class="btn btn-xs btn-info front-btn top-buffer" href="mailto:gerard@cosum.de?Subject=[Cosum] Kontakt" target="_top">gerard(at)cosum.de</a> <br />
        <a class="btn btn-xs btn-success front-btn top-buffer" href="http://cosum-blog.de">Zum Blog</a>
        </p>

        <a class="btn btn-lg btn-info front-btn top-buffer" href="/impressum" target="_top">Impressum</a>
        <a class="btn btn-lg btn-info front-btn top-buffer" href="https://gitlab.com/dialogium/cosum" target="_top">OpenSource!</a>
        <div class="alert alert-info"><strong>Datenschutzerklärung:</strong> <br /><a href="/dokumente/datenschutz_v2.pdf">datenschutz_v2.pdf</a></div>
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
            <h2>Empfohlen von</h2>
            <a href="https://www.bund-berlin.de/">
                <img style="width:50%" src="/img/bund_weiss_transp.png">
            </a>
            <h2>Teil von</h2>
            <img style="width:50%" src="/img/reuse-berlin.png">
            <a href="http://zero-waste-berlin.de/" class="">
                <img style="width:50%" src="/img/zero_waste.png">
            </a>
        </div>

        <div class="col-md-4 col-lg-4">
            <div class=".top-buffer"></div>
            <h2>Gefördert durch</h2>
            <a href="https://www.klimaschutz.de/" class="supporter">
                <img style="width:100%;" src="/img/bmub_nki.jpg">
            </a>
            <a href="https://www.stiftung-naturschutz.de/startseite/" class="supporter">
                <img style="width:50%" src="/img/stiftung_naturschutz.jpg">
            </a>


        </div>
        <p>&nbsp;</p>
    </div>
</div>
