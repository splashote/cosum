@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Orte</div>

                <div class="panel-body">
                    <a href="/orte/create" class="btn btn-primary">Ort anlegen</a>
                    <h3>Orte</h3>
                    @if(count($orte) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($orte as $ort)
                                <tr>
                                    <td>{{$ort->name}}</td>
                                    <td><a href="/orte/{{$ort->id}}/edit" class="btn btn-default">bearbeiten</a></td>
                                    <td>
                                        {!!Form::open(['action' => ['OrteController@destroy', $ort->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('löschen', ['class' => 'btn btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>Keine Orte</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
