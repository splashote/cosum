@extends('layouts.app')

@section('content')
    <h1>Ort bearbeiten</h1>
    {!! Form::open(['action' => ['OrteController@update', $ort->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', $ort->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('plz', 'PLZ')}}
            {{Form::number('plz', $ort->plz, ['class' => 'form-control', 'placeholder' => 'PLZ'])}}
        </div>

        <div class="form-group">
            {{Form::label('beschreibung', 'Beschreibung')}}
            {{Form::textarea('beschreibung', $ort->beschreibung, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Beschreibung Text'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Speichern', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
