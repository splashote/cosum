@extends ('layouts.app')

@section ('content')
    <h1>Ort anlegen</h1>
    {!! Form::open(['action' => 'OrteController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('plz', 'PLZ')}}
            {{Form::number('plz', '' , ['class' => 'form-control', 'placeholder' => 'PLZ'])}}
        </div>

        <div class="form-group">
            {{Form::label('beschreibung', 'Beschreibung')}} <i>&nbsp; Bitte Kontaktmöglichkeit angeben. </i>
            {{Form::textarea('beschreibung', '' , ['id' => 'article-ckeditor', 'class' => 'form-control' ])}}
        </div>
        {{Form::submit('anlegen', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
