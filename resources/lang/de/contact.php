<?php

// resources/lang/de/contact.php

return [
    'status.initial' => "erwünscht", 
    'status.pending' => "aufnehmen", 
    'status.made' => "verbunden" 
];
