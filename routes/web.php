<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/hello', function () {
    //return view('welcome');
    return '<h1>Hello World</h1>';
});

Route::get('/users/{id}/{name}', function($id, $name){
    return 'This is user '.$name.' with an id of '.$id;
});
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');
Route::get('/impressum', 'PagesController@impressum');

//Route::resource('posts', 'PostsController');
Route::resource('/proposals','ProposalsController');
Route::resource('/infotexts','InfotextsController');
Route::resource('/gesuche','ItemRequestsController');

Route::get('/dashboard', 'DashboardController@index');

//Route::resource('profil', 'ProfileController');
Route::get('profil','ProfileController@show');
Route::get('profil/edit','ProfileController@edit');
Route::post('profil/email','ProfileController@email_set');
Route::post('profil/update','ProfileController@update');

Route::resource('kontakte', 'ContactsController');
Route::get('kontakt/select', 'ContactsController@select');
Route::post('kontakt/select', 'ContactsController@select');
Route::resource('kontaktkreise', 'ContactCirclesController');
Route::post('kontaktkreise/{kontaktkreise}/add', 'ContactCirclesController@add');
Route::get('kontaktkreise/{kontaktkreise}/remove/{contact}', 'ContactCirclesController@remove');

Route::resource('orte', 'OrteController');
Route::resource('gegenstaende', 'GegenstaendeController');
Route::get('kommunal_gegenstaende', 'GegenstaendeController@index_member');
Route::get('meine_gegenstaende', 'GegenstaendeController@my_index');
//Route::resource('sichtbarkeiten', 'SichtbarkeitenController');
Route::get('gegenstaende/{id}/offenlegen', 'GegenstaendeController@reveal');
Route::get('offenlegungen/kreis/{circle_id}', 'RevelationController@circle');
Route::post('offenlegungen/save/circle', 'RevelationController@store_from_circle');
Route::resource('offenlegungen', 'RevelationController');
Route::get('meine_offenlegungen', 'RevelationController@my_revelations');
Route::delete('meine_offenlegungen/{revelation}', 'RevelationController@destroy');

Route::get('search','SearchController@go');
Route::post('search','SearchController@go');

Route::resource('regkeys','RegkeysController');

Route::get('/tos/latest','Auth\MyTosController@latest');
Route::get('/tos/agree/{tid}','Auth\MyTosController@agree');

Auth::routes(['verify' => true]);
