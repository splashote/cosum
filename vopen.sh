#!/usr/bin/env bash

#app/
#app/Http/Controllers
#resources/views/

search="$1";

FILES=$( /usr/bin/find app/ resources/views -iname $search | xargs)

vim -p $FILES
