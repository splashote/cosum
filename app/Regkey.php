<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regkey extends Model
{
    // Table Name
    protected $table = 'regkeys';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    public function creator(){
        return $this->belongsTo('App\User');
    }

    public function regusers(){
        return $this->belongsToMany('App\User', 'keyhooks', 'regkey_id','user_id')->withTimestamps();
    }

    public function auto_connectable_users(){
        $ousers=$this->regusers()->where('auto_connect',true);

        return $ousers;
    }

    public function auto_connect(User $user){
        $auto_users=$this->auto_connectable_users()->get();
        $result=true;
        //auto connect this user to all other users of this community
        foreach($auto_users as $auser){
            $result= $result && $user->auto_connect($auser);
        }
        return $result;
    }
}
