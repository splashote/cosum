<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Gegenstand;
use DB;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }

    public function go(Request $request){
        $gegenstaende=[];
        if ( ($request->search) != "")
        {
            $gegenstaende= Gegenstand::where('dispersion','<>','private')
                ->where(function ($query) use ($request){
                    $query->where('name', 'LIKE', "%$request->search%")
                          ->orWhere('beschreibung', 'LIKE', "%$request->search%")
                          ->orWhere('category', 'LIKE', "%$request->search%");
                })->orderBy('created_at','desc')->paginate(10);
        }else
            $gegenstaende= Gegenstand::where('dispersion','<>','private')
                                ->orderBy('created_at','desc')->paginate(10);

        return view('search.go')->with(['search' => $request->search, 'gegenstaende' => $gegenstaende]);
    }
}
