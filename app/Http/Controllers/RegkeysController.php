<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Regkey;
use DB;

class RegkeysController extends Controller
{
    protected $view_source='';//blueboard::';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'operator-user']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regkeys = Regkey::orderBy('created_at','desc')->paginate(10);
        return view($this->view_source.'regkeys.index')->with('regkeys', $regkeys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view_source.'regkeys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'keyname' => 'required',
            'keycode' => 'required',
        ]);

        // Create Regkey
        $regkey = new Regkey;
        $regkey->keyname = $request->input('keyname');
        $regkey->keycode = $request->input('keycode');
        $regkey->description = $request->input('description');
        $regkey->creator_id = auth()->user()->id;
        $regkey->save();

        return redirect('/regkeys')->with('success', 'Schlüssel hinzugefügt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $regkey = Regkey::find($id);
        return view($this->view_source.'regkeys.show')->with('regkey', $regkey);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regkey = Regkey::find($id);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Seite');
        }

        return view($this->view_source.'regkeys.edit')->with('regkey', $regkey);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'keyname' => 'required',
            'keycode' => 'required'
        ]);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }

        // Create Regkey
        $regkey = Regkey::find($id);
        $regkey->keyname = $request->input('keyname');
        $regkey->keycode = $request->input('keycode');
        $regkey->description = $request->input('description');
        $regkey->save();

        return redirect('/regkeys')->with('success', 'Schlüssel geändert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $regkey = Regkey::find($id);

        // Check for correct permission
        if(auth()->user()->isNotOperator()){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }

        $regkey->delete();
        return redirect('/regkeys')->with('success', 'Schlüssel gelöscht');
    }
}
