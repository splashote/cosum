<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\ItemRequest;
use DB;

class ItemRequestsController extends Controller
{
    protected $view_source='';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemrequests = ItemRequest::orderBy('created_at','desc')->paginate(10);
        return view($this->view_source.'itemrequests.index')->with('itemrequests', $itemrequests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view_source.'itemrequests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        // Create ItemRequest
        $itemrequest = new ItemRequest;
        $itemrequest->title = $request->input('title');
        $itemrequest->body = $request->input('body');
        $itemrequest->user_id = auth()->user()->id;
        $itemrequest->save();

        return redirect('/gesuche')->with('success', 'Gesuch hinzugefügt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itemrequest = ItemRequest::find($id);
        return view($this->view_source.'itemrequests.show')->with('itemrequest', $itemrequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemrequest = ItemRequest::find($id);

        // Check for correct permission
        if(auth()->user()->id != $itemrequest->user_id){
            return redirect()->back()->with('error', 'Unerlaubte Seite');
        }

        return view($this->view_source.'itemrequests.edit')->with('itemrequest', $itemrequest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        // find ItemRequest
        $itemrequest = ItemRequest::find($id);

        // Check for correct permission
        if(!$itemrequest || auth()->user()->id != $itemrequest->user_id){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }

        $itemrequest->title = $request->input('title');
        $itemrequest->body = $request->input('body');
        $itemrequest->user_id= auth()->user()->id;
        $itemrequest->save();

        return redirect('/gesuche')->with('success', 'Gesuch verändert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemrequest = ItemRequest::find($id);

        // Check for correct permission
        if(auth()->user()->id != $itemrequest->user_id){
            return redirect()->back()->with('error', 'Unerlaubte Aktion');
        }
 
        $itemrequest->delete();
        return redirect('/gesuche')->with('success', 'Gesuch gelöscht');
    }
}
