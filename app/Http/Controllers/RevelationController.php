<?php

namespace App\Http\Controllers;

use App\Revelation;
use App\Contact;
use App\Gegenstand;
use App\ContactCircle;
use Illuminate\Http\Request;

class RevelationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $revelations= Revelation::where('rcpt_user_id', auth()->user()->id)->get();
        foreach($revelations as $revelation){
            $revelation->obj = Gegenstand::find($revelation->obj_id);
		if ($revelation->obj && $revelation->obj->lent_to_date){
		    $ldate=new \DateTime($revelation->obj->lent_to_date);
		    $revelation->obj->lent_to_date= $ldate->format('d.m.Y');
		}
            $revelation->contact = Contact::find($revelation->rcpt_id);
            if ($revelation->contact->correspondent_id){
                $revelation->sender = Contact::where('correspondent_id', $revelation->sender_id)->where('owner_id', $revelation->contact->correspondent_id)->first();
                $revelation->subj_sender_name = "--";
                if ($revelation->sender)
                    $revelation->subj_sender_name = $revelation->sender->called;
            }
        }
        return view('revelations.index')->with('revelations', $revelations);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_revelations()
    {
        $revelations= Revelation::where('sender_id', auth()->user()->id)->get();
        foreach($revelations as $revelation){
            $revelation->obj = Gegenstand::find($revelation->obj_id);
                if ($revelation->obj && $revelation->obj->lent_to_date){
		    $ldate=new \DateTime($revelation->obj->lent_to_date);
		    $revelation->obj->lent_to_date= $ldate->format('d.m.Y');
		}
            $revelation->contact = Contact::find($revelation->rcpt_id);
        }
        return view('revelations.my_revelations')->with('revelations', $revelations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $gegenstand = Gegenstand::find($request->obj_id);
        $rcpt= Contact::find($request->rcpt_id);
            //render view create
            return view('revelations.create')->with(['rcpt' => $rcpt, 'gegenstand' => $gegenstand ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function circle(Request $request,$circle_id)
    {
        $gegenstand = Gegenstand::find($request->obj_id);
        $contactcircle= ContactCircle::find($circle_id);
            //render view create
            return view('revelations.create_circle')->with(['contactcircle' => $contactcircle, 'gegenstand' => $gegenstand ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $revelation = new Revelation;
        $revelation->sender_id = auth()->user()->id;
        $revelation->obj_id = $request->obj_id;
        $revelation->rcpt_id = $request->rcpt_id;
        $revelation->rcpt_user_id = Contact::find($request->rcpt_id)->correspondent_id;
        if (!$revelation->rcpt_user_id) {
            return back()->with('error', 'Kontakt muss verbunden sein!');
        }
        $hasAlready = Revelation::where('obj_id',$request->obj_id)->where('rcpt_id',$request->rcpt_id)->first();
        if ($hasAlready){
            return back()->with('error', 'Gegenstand diesem Kontakt bereits offengelegt!');
        }

        $revelation->save();
        return redirect('offenlegungen')->with('message', 'Gegenstand offengelegt!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_from_circle(Request $request)
    {
        $sender_id = auth()->user()->id;
        $obj_id = $request->obj_id;
        $contactcircle = auth()->user()->contactcircles()->where('id',$request->circle_id)->first();
        $contacts = $contactcircle->contacts()->get();
        $made_contacts=[];

        foreach($contacts as $contact){
            $contact->status= $contact->status();
            if ( $contact->status == 'made' ){
                array_push($made_contacts, $contact);
            }
        }
        $num_savings=0;
        $already_in=0;
        foreach ($made_contacts as $contact){
            $revelation = new Revelation;
            $revelation->sender_id = auth()->user()->id;
            $revelation->obj_id = $request->obj_id;
            $revelation->rcpt_id = $contact->id;
            $revelation->rcpt_user_id = $contact->correspondent_id;
            $hasAlready = Revelation::where('obj_id',$request->obj_id)->where('rcpt_id',$revelation->rcpt_id )->first();
            if (!$hasAlready){
                $revelation->save();
                $num_savings++;
            }else{
                $already_in++;
            }
        }

        return redirect('/gegenstaende/'.$obj_id.'/edit')->with('success', count($made_contacts) .' Kontakte aus Kreis '. $contactcircle->name  .' verbunden. '.$already_in.' bereits bestehende Offenlegungen. '. $num_savings.' Offenlegungen hinzugefügt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Revelation  $revelation
     * @return \Illuminate\Http\Response
     */
    public function show(Revelation $revelation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Revelation  $revelation
     * @return \Illuminate\Http\Response
     */
    public function edit(Revelation $revelation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Revelation  $revelation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Revelation $revelation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Revelation  $revelation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Revelation $revelation)
    {

        // Check for correct user
        if(auth()->user()->id !=$revelation->sender_id){
            return back()->with('error', 'Rücknahme nicht erlaubt.');
        }

        $revelation->forceDelete();
        return back()->with('success', 'Offenlegung zurückgenommen.');
    }
}
