<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Lendingprocess;
use Carbon\Carbon;
use App\CO2;
use App\Profile;

class ProfileController extends Controller
{
     protected $co2;
     protected $redirectTo = '/profil';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
        $this->co2= new CO2();
    }


    public function show(){
        $lendingprocesses=auth()->user()->lendingprocesses()->get();
        $multiplier=CO2::CO2_FACTOR;
        $this->co2->calc($lendingprocesses);

        return view('profile.show')->with(['lendingprocesses' => $lendingprocesses, 'lendingdays' => $this->co2->days, 'multiplier' => $multiplier , 'total_savings' => $this->co2->total_savings ]);
    }

    public function email_set(Request $request){
        $user=auth()->user();
        $user_id=$user->id;
        Log::notice('User '.$user_id.' requests email change from '.$request->input('old_email').' to '.$request->input('email'));
        $this->email_validator($request->all())->validate();

        $user->email= $request->input('email');
        $user->email_verified_at= null;
        $user->save();
        Log::notice('User '.$user_id.' successfully changed email from '.$request->input('old_email').' to '.$request->input('email'));
        $user->sendEmailVerificationNotification();

        return redirect('/profil')->with('success','Email geändert.');
    }

    public function update(Request $request){
        $user=auth()->user();
        $user_id=$user->id;
        $this->validator($request->all())->validate();
        $descr=$request->input('Beschreibungstext');
        $profile=$user->profile();
        $profile->update(['description' => $descr]);

        return redirect('/profil')->with('success','Profil aktualisiert.');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function email_validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|confirmed|string|email|max:255|unique:users',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Beschreibungstext' => 'string|max:400'
        ]);
    }

    public function edit(){
        $profile=auth()->user()->profile;
        return view('profile.edit')->with(['profile' => $profile]);
    }

}
