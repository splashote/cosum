<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactCircle;
use App\User;
use App\Contact;
use DB;

class ContactCirclesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['logs-out-banned-user','auth','verified','tos']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('contactcircles.index')->with('contactcircles', $user->contactcircles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contactcircles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	$this->validate($request, [
            'name' => 'required',
        ]);

        // Create ContactCircle
        $contactcircle = new ContactCircle;
        $contactcircle->name = $request->input('name');
        $contactcircle->user_id = auth()->user()->id;
        $contactcircle->save();

        return redirect('kontaktkreise')->with('success', 'Kontaktkreis angelegt.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactcircle = ContactCircle::find($id);
        return view('contactcircles.show')->with('contactcircle', $contactcircle);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contactcircle = ContactCircle::find($id);

        // Check for correct user
        if(auth()->user()->id !=$contactcircle->user_id){
            return redirect('kontaktkreise')->with('error', 'Unauthorized Page');
        }
        return view('contactcircles.edit')->with(['contactcircle'=> $contactcircle,'contacts' => $contactcircle->contacts()->get()] );
    }
    
    /**
    * Add Contact to Contactcircle.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request, $id){
        $contactcircle = ContactCircle::find($id);
        $message="Kontaktkreis nicht gefunden.";
        $message_type='error';
        if ($contactcircle == null){
            return redirect("/kontaktkreise/$id/edit")->with([$message_type => $message]);
        }
        // Check for correct user
        if(auth()->user()->id !=$contactcircle->user_id){
            return redirect('kontaktkreise')->with('error', 'Unauthorized Page');
        }

        if($request->new_contact_id != ""){
            $new_contact=Contact::where('id', $request->new_contact_id)->where('owner_id',auth()->user()->id)->first();
            if ($new_contact == null){
                $message="Kontakt nicht gefunden.";
                return redirect("/kontaktkreise/$id/edit")->with([$message_type => $message]);
            }
            if (!($contactcircle->contacts()->get()->contains($new_contact->id))){
                $contactcircle->contacts()->attach($new_contact);
                $message="$new_contact->called hinzugefügt.";
                $message_type='success';
            }else{
                $message="$new_contact->called bereits hinzugefügt.";
                $message_type='info';
            }

        }
        return redirect("/kontaktkreise/$id/edit")->with([$message_type => $message]);
    }

    /**
     * Remove Contact from ContactCircle.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $cid
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id, $cid){
        $contactcircle = ContactCircle::find($id);

        $message="Kontaktkreis nicht gefunden.";
        $message_type='error';
        if ($contactcircle == null){
            return redirect("/kontaktkreise/$id/edit")->with([$message_type => $message]);
        }

        // Check for correct user
        if(auth()->user()->id !=$contactcircle->user_id){
            return redirect('kontaktkreise')->with('error', 'Unauthorized Page');
        }

        if($cid != ""){
            $contact=Contact::where('id', $cid)->where('owner_id',auth()->user()->id)->first();
            if ($contact != null){
                $contactcircle->contacts()->detach($contact);
                $message="$contact->called aus dem Kreis genommen.";
                $message_type='success';
            }else{
                $message="Kontakt nicht enhalten.";
                $message_type='info';
            }
        }
        return redirect("/kontaktkreise/$id/edit")->with([$message_type => $message]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
	$this->validate($request, [
            'name' => 'required',
        ]);

	//update ContactCircle
        $contactcircle = ContactCircle::find($id);
        $contactcircle->name = $request->input('name');

	$contactcircle->save();
        return redirect('kontaktkreise')->with('success', 'Kontaktkreis aktualisiert:');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
	$contactcircle = ContactCircle::find($id);

        // Check for correct user
        if(auth()->user()->id !=$contactcircle->user_id){
            return redirect('kontaktkreise')->with('error', 'Unauthorized Page');
        }

        $contactcircle->delete();
        return redirect('kontaktkreise')->with('success', 'Kontaktkreis gelöscht');
    }
}
