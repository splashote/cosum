<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;
use App\Notifications\VerifyEmail;
use Cog\Contracts\Ban\Bannable as BannableContract;
use Cog\Laravel\Ban\Traits\Bannable;
use Dialogium\OperatorMiddleware\Contracts\Operatorable as OperatorContract;
use Dialogium\OperatorMiddleware\Traits\Operatorable;
use Dialogium\tos\Contracts\Auth\MustAgreeToS;
use Dialogium\tos\Auth\agreesToS;

class User extends Authenticatable implements MustVerifyEmail,MustAgreeToS,BannableContract,OperatorContract
{
    use Notifiable;
    use Bannable;
    use Operatorable;
    use agreesToS;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'auto_connect'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function itemRequests(){
        return $this->hasMany('App\ItemRequest');
    }

    public function kontakte(){
        return $this->hasMany('App\Kontakt');
    }

    public function contacts(){
        return $this->hasMany('App\Contact', 'owner_id');
    }

    public function regkey(){
        return $this->belongsToMany('App\Regkey', 'keyhooks','user_id','regkey_id')->withTimestamps()->first();
    }

    public function subjgruppen(){
        return $this->hasMany('App\SubjGruppe');
    }

    public function orte(){
        return $this->hasMany('App\Ort');
    }

    public function contactcircles(){
        return $this->hasMany('App\ContactCircle');
    }

    public function lendingprocesses(){
        return $this->hasMany('App\Lendingprocess');
    }

    public function gegenstaende(){
        return $this->hasMany('App\Gegenstand');
    }

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function sendEmailVerificationNotification(){
            $this->notify(new \App\Notifications\VerifyEmail());
    }

    public function sendPasswordResetNotification($token) {
            $this->notify(new \App\Notifications\MyResetPassword($token));
    }

    public function is_connected_to(User $user){
       return ($user && $this->contacts()->pluck('correspondent_id')->contains($user->id));
    }

    public function auto_connect(User $user){
        if (!$user) {
            return false;
        }

        //prevent connection to himself
        if ($this->is($user)){
            return true;
        }

        //already among contacts
        if ($this->is_connected_to($user)){
            return true;
        }

        $c1=new Contact;
        $c1->called = $user->name;
        $c1->owner_id = $this->id;
        $c1->save();

        $c2=new Contact;
        $c2->called = $this->name;
        $c2->owner_id = $user->id;
        $c2->save();

        $c1->auto_connect($c2);

        return true;
    }
}
