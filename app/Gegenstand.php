<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gegenstand extends Model
{
    // Table Name
    protected $table = 'gegenstands';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\User');
    }

}
