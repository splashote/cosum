<?php

namespace Dialogium\Blueboard\Http\Controllers;

use Illuminate\Http\Request;
use LendingprocessClassAlias;
use GegenstandClassAlias;
use RevelationClassAlias;

class ItemManagementController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','operator-user']);
    }

    public function items(){
        $items=GegenstandClassAlias::all();
        $blocked_items=GegenstandClassAlias::onlyTrashed()->get();
        $num_items=$items->count();
        $num_blocked=$blocked_items->count();

        return view('blueboard::items')->with([
                'items' => $items,
                'blocked_items' => $blocked_items,
                'num_blocked' => $num_blocked,
                'num_items' => $num_items
            ]);
    }

    /*
     * @param string $id
     *
     * */
    public function manage($id){
        $item=GegenstandClassAlias::find($id);
        if ($item == NULL){ 
            $item = GegenstandClassAlias::onlyTrashed()->where('id',$id)->first();
        }
        return view('blueboard::item_manage')->with(['item' => $item]);
    }

    public function block_post(Request $request)
    {
        $input = $request->all();
        $item_id = $input["item_id"];
        $item=GegenstandClassAlias::find($item_id);

        //revelations first
        $revelations_assoc= RevelationClassAlias::where('obj_id',$item_id)->get();
        foreach($revelations_assoc as $rev){
            $rev->delete(); //softdeletes
        }

        $item->delete();

        return response()->json(['success'=>'Gegenstand <strong>'. $item->name .'</strong> ('.$item->id.') gesperrt!']);
    }

    public function unblock_post(Request $request)
    {
        $input = $request->all();
        $item_id=$input["item_id"];
        $item = GegenstandClassAlias::onlyTrashed()->where('id',$item_id)->first();
        
        //revelations first
        $revelations_assoc= RevelationClassAlias::onlyTrashed()->where('obj_id',$item_id)->get();
        foreach($revelations_assoc as $rev){
            $rev->restore(); //undo softdeletes
        }

        $item->restore();

        return response()->json(['success'=>'Gegenstand <strong>'. $item->name .'</strong> ('.$item->id.') entsperrt!']);
    }

}
