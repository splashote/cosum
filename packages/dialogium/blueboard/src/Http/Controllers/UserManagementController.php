<?php

namespace Dialogium\Blueboard\Http\Controllers;

use Illuminate\Http\Request;
use TOSClassAlias;
use RegkeyClassAlias;
use LendingprocessClassAlias;
use GegenstandClassAlias;
use DispersionClassAlias;
use App\User;

class UserManagementController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','operator-user']);
    }

    public function index(){
        $users=User::all();
        $num_users=$users->count();

        return view('blueboard::users')->with([
                'users' => $users,
                'num_users' => $num_users
            ]);
    }

    public function banned_users(){
        $users = User::onlyBanned()->get();
        return $users;
    }

    /*
     * @param string $id
     *
     * */
    public function manage($id){
        $user=User::find($id);
        return view('blueboard::user_manage')->with(['user' => $user]);
    }

    public function ban_post(Request $request)
    {
        $input = $request->all();
        $user_id = $input["user_id"];
        $user=User::find($user_id);

        if ($user->isOperator()){
            return response()->json(['error'=>'Benutzer <strong>'. $user->email .'</strong> ('.$user->id.') kann nicht gesperrt werden!']);
        }

        $user->ban();
        $user->save();

        return response()->json(['success'=>'Benutzer <strong>'. $user->email .'</strong> ('.$user->id.') gesperrt!']);
    }

    public function unban_post(Request $request)
    {
        $input = $request->all();
        $user_id=$input["user_id"];
        $user=User::find($user_id);

        $user->unban();
        $user->save();
        return response()->json(['success'=>'Benutzer <strong>'. $user->email .'</strong> ('.$user->id.') entsperrt!']);
    }

}
