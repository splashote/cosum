@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')
<script src="/js/jquery-1.12.4.js"></script>
@section ('content')
              <h1>Gegenstands-Verwaltung</h1>
              <div class="row alert alert-warning">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <a href="/blueboard/" class="btn btn-sm btn-warning front-btn">Zurück</a>
                </div>
              </div>
                <div class="tab">
                  <button class="tablinks" onclick="openTab(event, 'items')" id="defaultOpen"><strong>Gegenstände ({{ $num_items }})</strong></button>
                  <button class="tablinks" onclick="openTab(event, 'blocked')"><strong>gesperrte ({{ $num_blocked }})</strong></button>
                  <button class="tablinks" onclick="openTab(event, 'reported')" disabled="disabled">gemeldete</button>
                </div>
 
                <div id="blocked" class="tabcontent">
                @foreach ( $blocked_items as $item )
                        <div class="row jumbotron">
                            <div class="col-md-4 col-sm-4">
                            {{$item->name}} ({{$item->id}})
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <img style="width:100%" src="/storage/cover_images/{{$item->cover_image}}">
                            </div>
                            <div class="col-md-1 col-sm-1">
                                <a href="/blueboard/item_manage/{{$item->id}}" name="btn-manage" id="" class="glyphicon glyphicon-pencil"></a>
                            </div>
                        </div>
                @endforeach
                </div>

                <div id="reported" class="tabcontent">
                </div>

              <div id="items" class="tabcontent">
              @foreach ( $items as $item )
                    <div class="row jumbotron">
                        <div class="col-md-4 col-sm-4">
                        {{$item->name}} ({{$item->id}})
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <img style="width:100%" src="/storage/cover_images/{{$item->cover_image}}">
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/blueboard/item_manage/{{$item->id}}" name="btn-manage" id="" class="glyphicon glyphicon-pencil"></a>
                        </div>
                    </div>
            @endforeach
            </div>
@endsection
@section ('page_script_code')

    document.getElementById("defaultOpen").click();

    function openTab(evt, tabName) {
      // Declare all variables
      var i, tabcontent, tablinks;

      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }

      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }

      document.getElementById(tabName).style.display = "block";
      evt.currentTarget.className += " active";
    }

@endsection

