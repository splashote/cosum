@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')
<script src="/js/jquery-1.12.4.js"></script>
@section ('content')
              <h1>Gegenstand verwalten</h1>
              <div class="row alert alert-warning">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <a href="/blueboard/item_management/" class="btn btn-sm btn-warning front-btn">Zurück</a>
                </div>
              </div>

              <div id="msg-container" class="row alert alert-info" hidden="hidden">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <p id="msg-text"></p>
                </div>
              </div>
              <div class="row jumbotron">
                    <form id="f_{{$item->id}}">
                        <div class="col-md-4 col-sm-4">
                            <strong>{{$item->name}} ({{$item->id}}) </strong>
                            <img style="width:100%" src="/storage/cover_images/{{$item->cover_image}}">
                        </div>
                        <div class="col-md-4 col-sm-4">
			    {!!$item->beschreibung!!}
                        </div>

                        <input type="text" name="item_id" id="item_id" value="{{$item->id}}"
                             hidden="hidden"
                        />

                        <div id="btn-unban-container" class="col-md-2 col-sm-2"
                        @if ($item->deleted_at == null)
                            hidden="hidden"
                        @endif
                        >
                            <input type="button" id="btn-unban" value="entsperren" class="form-control btn btn-sm btn-warning" />
                        </div>
                        <div id="btn-ban-container" class="col-md-2 col-sm-2"
                        @if ($item->deleted_at != null)
                            hidden="hidden"
                        @endif
                        >
                            <input type="button" id="btn-ban" value="sperren" class="form-control btn btn-sm btn-danger" />
                        </div>

                    </form>
              </div>
@endsection

@section ('page_script_code')
$( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#btn-unban").click(function(e){
        e.preventDefault();
        var action='unblock';
        gogogo(action);
    });

    $("#btn-ban").click(function(e){
        e.preventDefault();
        var action='block';
        gogogo(action);
    });

    function gogogo(action){
        var item_id = $("input[name=item_id]").val();
        console.log('item ' + item_id + ", action:" + action);

        $.ajax({
           type:'POST',
           url:'/blueboard/item_'+action+'_post',
           data:{item_id:item_id},
           success:function(data){
             $('#msg-container').show();
             if (data.success){
              update(action,data);
             }
             if (data.error){
              error(action,data);
             }
           }
        });

    }

    function error(action,data){
        $('#msg-text').html(data.error);
                var mc = $('#msg-container');
                    mc.addClass('alert-danger');
                    mc.removeClass('alert-info');
                    mc.removeClass('alert-warning');
                    mc.removeClass('alert-success');

        if (action =='block'){
            $('#btn-ban-container').hide();
            $('#btn-unban-container').show();
        }
        if (action =='unblock'){
            $('#btn-unban-container').hide();
            $('#btn-ban-container').show();
        }
    }

    function update(action,data){
        $('#msg-text').html(data.success);
                var mc = $('#msg-container');
                    mc.addClass('alert-success');
                    mc.removeClass('alert-info');
                    mc.removeClass('alert-danger');
                    mc.removeClass('alert-warning');

        if (action =='block'){
            $('#btn-ban-container').hide();
            $('#btn-unban-container').show();
        }
        if (action =='unblock'){
            $('#btn-unban-container').hide();
            $('#btn-ban-container').show();
        }
    }


});

@endsection

