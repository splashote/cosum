@extends ( View::exists('layouts.blueboard')?'layouts.blueboard' : 'blueboard::layouts.blueboard')

@section ('content')
              <h1>Benutzer-Verwaltung</h1>
              <div class="row alert alert-warning">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <a href="/blueboard" class="btn btn-sm btn-warning front-btn">Zurück</a>
                </div>
              </div>

              <div class="row jumbotron">
                    <h2>Benutzer</h2>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <p>{{ $num_users }}</p>
                    </div>
              </div>
              @foreach ( $users as $user )
                    <div class="row jumbotron">
                        <div class="col-md-5 col-sm-5">
                        {{$user->email}}
                        </div>
                        <div class="col-md-2 col-sm-2">
                        <i>{{$user->id}}</i>
                            @if ($user->isOperator())
                                <i class="glyphicon glyphicon-wrench"></i>
                            @endif
                            @if ($user->isBanned())
                                <i class="glyphicon glyphicon-alert"></i>
                            @endif
                        </div>

                        <div class="col-md-1 col-sm-1">
                            <a href="/blueboard/user_manage/{{$user->id}}" name="btn-manage" id="" class="glyphicon glyphicon-pencil"></a>
                        </div>
                    </div>
                    @endforeach
@endsection
