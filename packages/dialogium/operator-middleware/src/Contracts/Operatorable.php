<?php

namespace Dialogium\OperatorMiddleware\Contracts;

/**
 * Interface Operator.
 *
 * @package App\Contracts\Op
 */
interface Operatorable
{
    /**
     * Set operator flag.
     *
     * @return $this
     */
    public function setOperatorFlag();

    /**
     * Unset operator flag.
     *
     * @return $this
     */
    public function unsetOperatorFlag();

    /**
     * If model is operator.
     *
     * @return bool
     */
    public function isOperator();

    /**
     * If model is not operator.
     *
     * @return bool
     */
    public function isNotOperator();
}
